# Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os

CC = "avr-gcc"
CXX = "avr-g++"
AVRSIZE = "avr-size"
AVRDUDE = "avrdude"
OBJCOPY = "avr-objcopy"
OBJDUMP = "avr-objdump"
RANLIB = "avr-ranlib"
HEXFORMAT = "ihex"

# programmer id for avrdude
AVRDUDE_MCU = "m328p"

# programmer id for avrdude
AVRDUDE_PROGRAMMER = "arduino"

# baud rate for upload
AVRDUDE_BAUD = 57600

# serial port where device is, for uploading
AVRDUDE_PORT = "/dev/ttyUSB0"

# name of target
TARGET = "avr_project"

# libs to link into target
LIBS = [
	"m",
	"serial",
	"timing",
	"adc"
]

# include paths
INCLUDES = [
	"."
]

# preproc definitions
DEFINES = [
	"F_CPU=16000000UL",
	"DEBUG"
]

# name of target controller for (avr-gcc mmcu)
MCU = "atmega328p"

# optimization level: s(size), 0(none), 1, 2, or 3
OPTIMIZE = "s"

# CFLAGS
CFLAGS = [
	"-gstabs",
	"-mmcu=${MCU}",
	"-O${OPTIMIZE}",
	"-fpack-struct",
	"-fshort-enums",
	"-funsigned-char",
	"-funsigned-bitfields",
	"-Wall",
	"-Wstrict-prototypes",
	"-std=gnu99",
	"-Werror",
]

# c++ flags
CXXFLAGS = [
	"-fno-exceptions"
]

# linker flags
LDFLAGS = [
	"-mmcu=${MCU}"
]

# lib paths
LIBPATH = [
	".",
	"serial",
	"timing",
	"adc"
]


###############################################################################
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################

env = Environment(
	ENV = os.environ,
	CC = CC,
	CXX = CXX,
	AVRSIZE = AVRSIZE,
	OBJCOPY = OBJCOPY,
	OBJDUMP = OBJDUMP,
	AVRDUDE = AVRDUDE,
	AVRDUDE_PORT = AVRDUDE_PORT,
	AVRDUDE_PROGRAMMER = AVRDUDE_PROGRAMMER,
	AVRDUDE_MCU = AVRDUDE_MCU,
	AVRDUDE_BAUD = AVRDUDE_BAUD,
	HEXFORMAT = HEXFORMAT,
	RANLIB = RANLIB,
	BIN_NAME = TARGET,
	CPPPATH = INCLUDES,
	CPPDEFINES = DEFINES,
	CCFLAGS = CFLAGS,
	CXXFLAGS = CXXFLAGS,
	LINKFLAGS = LDFLAGS,
	LIBS = LIBS,
	LIBPATH = LIBPATH,
	MCU = MCU,
	OPTIMIZE = OPTIMIZE,
	TARFLAGS = "-vcj",
	TARSUFFIX = ".tbz2"
)

env.Append(BUILDERS = {
	"HEX": Builder(action = "${OBJCOPY} -O ${HEXFORMAT} -R .eeprom $SOURCE $TARGET"),
	
	"HEX_EEP": Builder(action = "${OBJCOPY} -j .eeprom --set-section-flags=.eeprom=\"alloc,load\" \
			--change-section-lma .eeprom=0 -O ${HEXFORMAT} $SOURCE $TARGET"),

	"DISASM": Builder(action = "${OBJDUMP} -S $SOURCE > $TARGET")
})

Export("env")
env.SConscript("src/SConscript", variant_dir = "build/${MCU}")
env.SConscript("src/serial/SConscript", variant_dir = "build/${MCU}/serial")
env.SConscript("src/timing/SConscript", variant_dir = "build/${MCU}/timing")
env.SConscript("src/adc/SConscript", variant_dir = "build/${MCU}/adc")


