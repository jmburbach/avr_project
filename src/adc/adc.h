/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef __adc_h__
#define __adc_h__

#include "../common.h"

static inline void adc_init(void);
static inline void adc_stop(void);

/* Channel should be between  0 and 5 for reading the general analogue inputs,
 * or 8 for reading the onboard temperature sensor. Will return -1 if an invalid
 * channel number given. When reading from channel 0-5, will use AVcc (+5v) as
 * the reference. When reading the onboard temperature sensor on channel 8, it
 * will use the internal 1.1v reference. */
extern int16_t adc_read_channel(uint8_t channel);

/******************************************************************************/

void adc_init(void)
{
	/* Enable adc with prescaler at 128. With 16Mhz clock this puts the adc
	 * clock at 125khz. Datasheet says between 50 and 200khz for maximum
	 * resolution */
	ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
}

void adc_stop(void)
{
	ADCSRA = 0;
	ADMUX = 0;
}

#endif // __adc_h__

