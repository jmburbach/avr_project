/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "adc.h"


int16_t adc_read_channel(uint8_t channel)
{
	uint8_t low_byte;
	uint8_t high_byte;

	/* sanity check */
	if (channel > 5 && channel != 8)
		return -1;

	/* channel 8 is the onboard temperature sensor, and datasheet
	 * says we need to use the internal 1.1v reference for that one. */
	if (channel == 8) {
		ADMUX = (1 << REFS1)  | (1 << REFS0) | channel;
	}
	else {
	   	/* will use AVcc (+5v) as reference for others */
		ADMUX = (1 << REFS0) | channel;
	}

	/* start the conversion */
	ADCSRA |= (1 << ADSC);

	/* ADSC stays set until conversion complete...wait for it to clear. */
	while ( ADCSRA & (1 << ADSC) ) ;

	/* must read low byte first */
	low_byte = ADCL;
	high_byte = ADCH;

	return (high_byte << 8) | low_byte;
}

