/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef __atomic_h__
#define __atomic_h__

#include "common.h"

/* Begin a section of code where all instructions following are guaranteed to be
 * atomic. Also performs a memory clobber to ensure no instructions are reordered
 * across this barrier. Takes a pointer into which it will save the current SREG
 * state, and which should also be passed to atomic_end unchanged when your done
 * with your critical section of code. */
static inline void atomic_begin(uint8_t* state) __attribute__((always_inline));

/* For every atomic_begin, there should be an atomic_end to match. The state 
 * pointer should be the same as passed to atomic_begin, and will restore the
 * state that was saved there. Also performs a memory clobber to ensure that
 * instructions don't get reordered across this barrier. */
static inline void atomic_end(const uint8_t* state) __attribute__((always_inline));


void atomic_begin(uint8_t* state)
{
	*state = SREG;
	__asm__ volatile ("cli" ::: "memory");
}

void atomic_end(const uint8_t* state)
{
	__asm__ volatile ("" ::: "memory");
	SREG = *state;
}

#endif // __atomic_h__

