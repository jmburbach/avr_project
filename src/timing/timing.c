/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "timing.h"
#include "../atomic.h"

static volatile uint32_t __timing_interrupt_count;


ISR(TIMER0_COMPA_vect)
{
	++__timing_interrupt_count;
}

void timing_init(void)
{
	/* ctc mode with prescaler at 64 */
	TCCR0A |= (1 << WGM01);
	TCCR0B |= (1 << CS01) | (1 << CS00);	
	
	/* set the compare value */
	/* with 16 mhz clock and prescaler at 64 this
	 * should be 1ms per interrupt */
	OCR0A = 249;
	
	/* enable timer0 compare match A interrupt */
	TIMSK0 |= (1 << OCIE0A);
}

void timing_stop(void)
{
	/* disable interrupt */
	TIMSK0 &= ~(1 << OCIE0A);

	/* reset everything */
	TCCR0A = 0;
	TCCR0B = 0;
	OCR0A = 0;
	__timing_interrupt_count = 0;
}

uint32_t timing_elapsed_ms(void)
{
	uint32_t ms;
	uint8_t state;

	atomic_begin(&state);
	
	ms = __timing_interrupt_count;

	atomic_end(&state);

	return ms;
}

uint32_t timing_elapsed_us(void)
{
	uint32_t ms;
	uint8_t count;
	uint8_t state;

	atomic_begin(&state);

	/* get current milliseconds */
	ms = __timing_interrupt_count;

	/* and current value of the counter */
	count = TCNT0;

	/* Check if match interrupt is pending, as the timer doesn't stop
	 * ticking just because we're in here. Add a millisecond if pending
	 * interrupt... */
	if ( (TIFR0 & (1 << OCF0A)) ) {
		++ms;
	}

	atomic_end(&state);

	/* calculate and return microseconds */
	return ((ms * 1000) + (count * (64 / CLOCK_CYCLES_PER_MICROSECOND)));
}

