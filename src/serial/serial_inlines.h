/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef __serial_inlines_h__
#define __serial_inlines_h__


uint8_t serial_rx_available(void)
{
	extern volatile uint8_t __serial_rx_read_pos;
	extern volatile uint8_t __serial_rx_write_pos;
	return ((SERIAL_RX_BUFFER_SIZE +
			   	(__serial_rx_write_pos - __serial_rx_read_pos))
				& SERIAL_RX_BUFFER_MASK);
}

uint8_t serial_tx_available(void)
{
	extern volatile uint8_t __serial_tx_read_pos;
	extern volatile uint8_t __serial_tx_write_pos;
	return ((SERIAL_TX_BUFFER_SIZE -
				(__serial_tx_write_pos - __serial_tx_read_pos) - 1)
				& SERIAL_TX_BUFFER_MASK);
}

#endif /* __serial_inlines_h__ */

