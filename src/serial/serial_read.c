/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "serial.h"

extern volatile uint8_t __serial_rx_read_pos;
extern uint8_t __serial_rx_buffer[SERIAL_RX_BUFFER_SIZE];


uint8_t serial_read(void* data, uint8_t count)
{
	uint8_t pos;
	uint8_t nbytes;
	uint8_t available;
	uint8_t* bufp = (uint8_t*)data;

	available = serial_rx_available();
	nbytes = count = min(available, count);

	if (count == 0)
		return 0;

	do {
		/* Important that __serial_rx_read_pos not get advanced until after
		 * the copy operation is fully completed...in case of an rx interrupt
		 * before the copy is complete. Otherwise the rx interrupt would see
		 * the space as free and possibly overwrite it before we finished
		 * copying it. */

		/* so copy current read pos */
		pos = __serial_rx_read_pos;
		
		/* and copy the current byte to `data'. */
		*bufp++ = __serial_rx_buffer[pos];

		/* and update the read pos only after the copy is fully complete. */
		__serial_rx_read_pos = SERIAL_RX_BUFFER_NEXT_INDEX(pos);
	} while (--count);

	return nbytes;
}

