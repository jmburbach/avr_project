/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "serial.h"

extern volatile uint8_t __serial_tx_write_pos;
extern uint8_t __serial_tx_buffer[SERIAL_TX_BUFFER_SIZE];


uint8_t serial_write(const void* data, uint8_t count)
{
	uint8_t pos;
	uint8_t nbytes;
	uint8_t available;
	const uint8_t* bufp = (const uint8_t*)data;

	available = serial_tx_available();
	nbytes = count = min(available, count);

	if (count == 0)
		return 0;
	

	do {
		/* Important that __serial_tx_write_pos not get advanced until after
		 * the copy operation is fully completed...in case of an tx interrupt
		 * before the copy is complete. Otherwise the tx interrupt would see the
		 * space as occupied and possibly transmit it before we actually
		 * finished copying it. */

		/* so copy current write pos */
		pos = __serial_tx_write_pos;
		
		/* and copy the byte into the transmit buffer */
		__serial_tx_buffer[pos] = *bufp++;

		/* and update the write pos only after the copy is fully complete. */
		__serial_tx_write_pos = SERIAL_TX_BUFFER_NEXT_INDEX(pos);
	} while (--count);

	/* make sure the interrupt is enabled */
	UCSR0B |= (1 << UDRIE0);

	return nbytes;
}

