/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "serial.h"

#include <stdio.h>
#include <stdarg.h>

extern volatile uint8_t __serial_tx_write_pos;
extern uint8_t __serial_tx_buffer[SERIAL_TX_BUFFER_SIZE];


uint8_t serial_write_formatted_string(const char* format, ...)
{
	int bytes;
	va_list ap;
	uint8_t available;
	uint8_t written;
	char buffer[SERIAL_TX_BUFFER_SIZE];
	
	available = serial_tx_available();
	if (available == 0)
		return 0;

	va_start(ap, format);
	bytes = vsnprintf(buffer, available, format, ap);
	va_end(ap);

	written = bytes >= available ? available : (uint8_t)bytes + 1;

	return serial_write(buffer, written);
}

