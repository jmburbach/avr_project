/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "serial.h"
#include "../atomic.h"

/* buffer to store received bytes */
uint8_t __serial_rx_buffer[SERIAL_RX_BUFFER_SIZE];

/* current read position in __serial_rx_buffer */
volatile uint8_t __serial_rx_read_pos;

/* current write position in __serial_rx_buffer */
volatile uint8_t __serial_rx_write_pos;

/* buffer to stored bytes to be transmitted */
uint8_t __serial_tx_buffer[SERIAL_TX_BUFFER_SIZE];

/* current read position in __serial_tx_buffer */
volatile uint8_t __serial_tx_read_pos;

/* current write position in __serial_tx_buffer */
volatile uint8_t __serial_tx_write_pos;


/* interrupt routine for receive */
ISR(USART_RX_vect)
{
	uint8_t current_write_pos;
	uint8_t next_write_pos;
	uint8_t data;

	/* copy to avoid volatile overhead */
	current_write_pos = __serial_rx_write_pos;
	next_write_pos = SERIAL_RX_BUFFER_NEXT_INDEX(current_write_pos);

	/* read the byte */
	data = UDR0;

	/* check if we are full */
	if (next_write_pos == __serial_rx_read_pos) {
		/* cannot fit any more until something gets read...discard */
		return; 
	}

	/* store the received data */
	__serial_rx_buffer[current_write_pos] = data;

	/* save the new position */
	__serial_rx_write_pos = next_write_pos;
}

/* interrupt routine for transmit */
ISR(USART_UDRE_vect)
{
	uint8_t current_read_pos;

	current_read_pos = __serial_tx_read_pos;

	/* disable interrupt if nothing left */
	if (current_read_pos == __serial_tx_write_pos) {
		UCSR0B &= ~(1 << UDRIE0);
		return;
	}

	/* start transmission of next byte */
	UDR0 = __serial_tx_buffer[current_read_pos];

	/* advance read pos */
	__serial_tx_read_pos = SERIAL_TX_BUFFER_NEXT_INDEX(current_read_pos);
}


int8_t serial_init(struct serial_params* params)
{
	uint8_t state;
	uint16_t ubrr0;
	uint8_t ucsr0c;

	ucsr0c = 0;
	
	/* no params then just go with defaults */
	if (params == NULL) {
		 /* 9600 bps, 8 bit, no parity, 1 stop bit */
		ubrr0 = SERIAL_UBRR_9600;
		ucsr0c |= (1 << UCSZ01) | (1 << UCSZ00);
		goto end_params;
	}

	/* check baud rate */
	switch (params->rate_bps) {
		case SERIAL_RATE_BPS_2400:
			ubrr0 = SERIAL_UBRR_2400;
			break;
		case SERIAL_RATE_BPS_4800:
			ubrr0 = SERIAL_UBRR_4800;
			break;
		case SERIAL_RATE_BPS_9600:
			ubrr0 = SERIAL_UBRR_9600;
			break;
		case SERIAL_RATE_BPS_14400:
			ubrr0 = SERIAL_UBRR_14400;
			break;
		case SERIAL_RATE_BPS_19200:
			ubrr0 = SERIAL_UBRR_19200;
			break;
		case SERIAL_RATE_BPS_28800:
			ubrr0 = SERIAL_UBRR_28800;
			break;
		case SERIAL_RATE_BPS_38400:
			ubrr0 = SERIAL_UBRR_38400;
			break;
		case SERIAL_RATE_BPS_57600:
			ubrr0 = SERIAL_UBRR_57600;
			break;
		case SERIAL_RATE_BPS_76800:
			ubrr0 = SERIAL_UBRR_76800;
			break;
		case SERIAL_RATE_BPS_115200:
			ubrr0 = SERIAL_UBRR_115200;
			break;
		default:
			return -1;
	}

	/* check parity */
	switch (params->parity) {
		case SERIAL_PARITY_NONE:
			break;
		case SERIAL_PARITY_EVEN:
			ucsr0c |= (1 << UPM01);
			break;
		case SERIAL_PARITY_ODD:
			ucsr0c |= (1 << UPM01) | (1 << UPM00);
			break;
		default:
			return -2;
	}
	
	/* stop bits */
	switch (params->stop_bits) {
		case SERIAL_STOP_BITS_1:
			ucsr0c &= ~(1 << USBS0);
			break;
		case SERIAL_STOP_BITS_2:
			ucsr0c |= (1 << USBS0);
			break;
		default:
			return -3;
	}
	
	/* always 8 bits for now */
	ucsr0c |= (1 << UCSZ01) | (1 << UCSZ00);

end_params:

	/* make sure interrupts are off until we are finished */
	atomic_begin(&state);
	
	/* make sure USART is enabled */
	PRR &= ~(1 << PRUSART0);

	/* load the baud */
	UBRR0H = (uint8_t)(ubrr0 >> 8);
	UBRR0L = (uint8_t)ubrr0;

	/* set the format */
	UCSR0C = ucsr0c;

	/* Enable receiver, transmitter, and interrupt for rx complete. Interrupt
	 * for tx will be enabled by the write functions and disabled by the UDRE
	 * interrupt itself when appropriate. */
	UCSR0B |= (1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0);
	
	/* reset buffer positions */
	__serial_rx_read_pos = 0;
	__serial_rx_write_pos = 0;
   	__serial_tx_read_pos = 0;
	__serial_tx_write_pos = 0;

	/* restore */
	atomic_end(&state);

	return 0;
}

