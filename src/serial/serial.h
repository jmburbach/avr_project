/* Copyright (c) 2011 Jacob M. Burbach <jmburbach@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef __serial_h__
#define __serial_h__

#include "../common.h"

/* include 'static' serial options */
#include "serial_config.h"

/* baud rate constants..for setting `rate_bps' in the `serial_params' struct */
#define SERIAL_RATE_BPS_2400	0
#define SERIAL_RATE_BPS_4800	1
#define SERIAL_RATE_BPS_9600	2
#define SERIAL_RATE_BPS_14400	3
#define SERIAL_RATE_BPS_19200	4
#define SERIAL_RATE_BPS_28800	5 
#define SERIAL_RATE_BPS_38400	6
#define SERIAL_RATE_BPS_57600	7
#define SERIAL_RATE_BPS_76800	8
#define SERIAL_RATE_BPS_115200	9

/* parity constants..for setting `parity' in the `serial_params' struct */
#define SERIAL_PARITY_NONE	0
#define SERIAL_PARITY_ODD	1
#define SERIAL_PARITY_EVEN	2

/* stop bit constants..for setting `stop_bits' in the `serial_params' struct */
#define SERIAL_STOP_BITS_1 0
#define SERIAL_STOP_BITS_2 1

/* Structure used to pass configuration options to serial_init.
 * Values should be set with the appropriate SERIAL_RATE_BPS_*, 
 * SERIAL_PARITY_*, or SERIAL_STOP_BITS_* constants only. */
struct serial_params {
	uint8_t rate_bps : 5;
	uint8_t parity : 2;
	uint8_t stop_bits : 1;
};

/* Initialize the serial system with desired `params'. If `params' is NULL
 * it will use default values (9600bps, no parity, 1 stop bits). Return 0
 * if successful, less than 0 if something is wrong with given params. */
extern int8_t serial_init(struct serial_params* params);

/* Query the number of bytes available for reading from the receive buffer */
static inline uint8_t serial_rx_available(void);

/* Request `count' bytes be read into `data'. Returns the
 * actual number of bytes read, which will always be the smaller
 * of `count' or the value returned from `serial_rx_available()'. */
extern uint8_t serial_read(void* data, uint8_t count);

/* Query the number of bytes available for writing in the transmit buffer */
static inline uint8_t serial_tx_available(void);

/* Request `count' bytes of `data' be written to the transmit buffer. Returns
 * the number of bytes actually written, which will always be the smaller of
 * `count' or the value returned from `serial_tx_available()' */
extern uint8_t serial_write(const void* data, uint8_t count);

/* Transmit a formatted string using the same formatting system as fprintf,
 * sprintf, etc. If the final size of the formatted string is larger than space
 * availabe in the transmit buffer, the output will be silently truncated.
 * Returns the number of bytes actually written. */
extern uint8_t serial_write_formatted_string(const char* format, ...);



/* define buffer mask values */
#define SERIAL_RX_BUFFER_MASK (SERIAL_RX_BUFFER_SIZE - 1)
#define SERIAL_TX_BUFFER_MASK (SERIAL_TX_BUFFER_SIZE - 1)

/* check the rx buffer size is proper */
#if ((SERIAL_RX_BUFFER_SIZE & SERIAL_RX_BUFFER_MASK) || \
		(SERIAL_RX_BUFFER_SIZE > 256) || (SERIAL_RX_BUFFER_SIZE < 0))
#	error "SERIAL_RX_BUFFER_SIZE is configured incorrectly."
#endif

/* check the tx buffer size is proper */
#if ((SERIAL_TX_BUFFER_SIZE & SERIAL_TX_BUFFER_MASK) || \
		(SERIAL_TX_BUFFER_SIZE > 256) || (SERIAL_TX_BUFFER_SIZE < 0))
#	error "SERIAL_TX_BUFFER_SIZE is configured incorrectly."
#endif

/* convenience macros to get next buffer index */
#define SERIAL_RX_BUFFER_NEXT_INDEX(X) (((X) + 1) & SERIAL_RX_BUFFER_MASK)
#define SERIAL_TX_BUFFER_NEXT_INDEX(X) (((X) + 1) & SERIAL_TX_BUFFER_MASK)

/* baud values for cpu clocks */
#if F_CPU == 16000000UL
#	define SERIAL_UBRR_2400		416	   // -0.1%
#	define SERIAL_UBRR_4800		207	   //  0.2%
#	define SERIAL_UBRR_9600		103	   //  0.2%
#	define SERIAL_UBRR_14400	 68	   //  0.6%
#	define SERIAL_UBRR_19200	 51	   //  0.2%
#	define SERIAL_UBRR_28800	 34	   // -0.8%
#	define SERIAL_UBRR_38400	 25	   //  0.2%
#	define SERIAL_UBRR_57600	 16	   //  2.1%
#	define SERIAL_UBRR_76800	 12	   //  0.2%
#	define SERIAL_UBRR_115200	  8	   // -3.5%
#else
#	error "serial currently only works with a clock rate of 16 MHz..sorry"
#endif

#include "serial_inlines.h" /* inline function definitions */

#endif // __serial_h__

